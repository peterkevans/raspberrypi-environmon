#!/bin/sh

# Copyright (c) 2015 Peter Evans <pete@cysec.net>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# Install packages
apt-get update && \
    apt-get upgrade && \
    apt-get install -y \
        build-essential \
        curl \
        git \
        nginx \
        python-dev \
        rpi-update \
        unzip \
        wget

# Install Adafruit Python DHT Library
cd /root
git clone -b master https://github.com/adafruit/Adafruit_Python_DHT.git Adafruit_Python_DHT
cd /root/Adafruit_Python_DHT
python /root/Adafruit_Python_DHT/setup.py install

# Install dygraphs charting library
mkdir -p /var/www/html/
cd /var/www/html/
wget "http://dygraphs.com/1.1.1/dygraph-combined.js"

# Copy in Payload files
cd /root/environmon/payload
cp index.html /var/www/html/
cp default /etc/nginx/sites-enabled/
cp environmon-log /etc/logrotate.d/
cp environmon-cron /etc/cron.d/
