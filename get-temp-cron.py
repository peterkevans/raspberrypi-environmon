#!/usr/bin/python

# Copyright (c) 2014 Adafruit Industries
# Author: Tony DiCola
# Modified: 2015 Peter K Evans <pete@cysec.net>

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import os
import sys
import time
import Adafruit_DHT

os.chdir('/root')

sensorOutput = [0.0, 0.0]
maxMin = '24.0,18.0,60.0,40.0' # Max Temp, Min Temp, Max Humid, Min Humid

# Sensor should be set to Adafruit_DHT.DHT11,
# Adafruit_DHT.DHT22, or Adafruit_DHT.AM2302.
sensor = Adafruit_DHT.AM2302

# Example using a Raspberry Pi with DHT sensor
# connected to GPIO23.
pin = 4

def gettemp():
	# Try to grab a sensor reading.  Use the read_retry method which will retry up
	# to 15 times to get a sensor reading (waiting 2 seconds between each retry).
	humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)

	# Note that sometimes you won't get a reading and
	# the results will be null (because Linux can't
	# guarantee the timing of calls to read the sensor).
	# If this happens try again!
	if humidity is not None and temperature is not None:
		return (temperature, humidity)
	else:
		return (0.0, 0.0)

timeStamp = time.strftime("%Y-%m-%d %H:%M:%S")
sensorOutputRaw = gettemp()
sensorOutput[0] = float(sensorOutputRaw[0]) + 0.0  # calibration
sensorOutput[1] = float(sensorOutputRaw[1]) + 0.0
resultString = timeStamp+','+'%.1f'%(float(sensorOutput[0]))+','+'%.1f'%(float(sensorOutput[1]))+','+maxMin
print(resultString)
