# Raspberry Pi Environment Monitor

[![https://gitlab.com/peterkevans/raspberrypi-environmon](https://img.shields.io/badge/gitlab-raspberrypi--environmon-b3003b.svg?style=flat-square)]()

> An Environment Monitoring Tool that uses a Raspberry Pi to monitor temperature and humidity, and then graph the results.


## Requirements
Hardware:

- [Raspberry Pi 2B](https://www.adafruit.com/products/2358) (has also been tested on Raspberry Pi 1B+ and Raspberry Pi 3B)
- A [DHT22](https://www.adafruit.com/products/385) or [AM2302 (wired DHT22)](https://www.adafruit.com/product/393) temperature and humidity sensor

N.B. The DHT22 requires a [4.7K - 10K resistor](https://www.adafruit.com/products/2784) (as a pull-up from the data pin to VCC) whereas the AM2302 does not as it already has a 5.1K resistor inside.

Operating System:

- Latest version of [Raspbian](https://www.raspberrypi.org/downloads/raspbian/) on a [good quality SD card](http://www.jeffgeerling.com/blogs/jeff-geerling/raspberry-pi-microsd-card). (last tested with "RASPBIAN JESSIE LITE")

Libraries:

- [Adafruit Python DHT Sensor Library](https://github.com/adafruit/Adafruit_Python_DHT.git) (last tested with commit 3f30544)
- [Dygraphs JavaScript Charting Library](http://dygraphs.com/index.html) (last tested with version 1.1.1)

N.B. You will not be required to install the libraries above yourself as the script included in the repository will take care of that for you.

## Sensor Assembly

Connect the temperature/humidity sensor as shown below:

DHT22:

![Raspberry Pi 2B with DHT22](docs/dht22-raspi-2B.png)

AM2302:

![Raspberry Pi 2B with AM2302](docs/am2302-raspi-2B.png)


## Software Setup

Firstly, connect a keyboard, monitor, mouse, and ethernet cable to the Raspberry Pi, then connect the power supply and turn on.

On first boot, run the `raspi-config` utility.

```bash
sudo raspi-config
```

Perform the following:

- Expand Filesystem
- Internationalisation Options
  - Change Locale = `en_US UTF-8`
  - Change Timezone
  - Change Keyboard Layout = `105-key (Intl) PC`, `English (US)`
  - Change Wi-fi Country
- Advanced Options
  - Hostname
  - Memory Split = `16M` as we're running headless
  - SSH = `enable`

Reboot when asked.

Check your keyboard is typing the correct characters and then set a strong password.

```bash
passwd pi
```


### Setup wifi

> Run all below commands in this section as root (`sudo bash`)

To scan for WiFi networks, use the command

```bash
iwlist wlan0 scan
```

To configure the network,

```bash
vi /etc/wpa_supplicant/wpa_supplicant.conf
```

Go to the bottom of the file and add the following:

```
network={
    ssid="ESSID_from_scan"
    psk="Your_wifi_password"
}
```

If your wifi does not connect shortly

```bash
ifdown wlan0
ifup wlan0
```

### SD Card Write Optimisation

>To prevent your Raspberry Pi from writing a lot of data, and thus, wearing out the SD card, follow the steps at [Raymii.org](https://raymii.org/s/blog/Broken_Corrupted_Raspberry_Pi_SD_Card.html) and [bsilverstrim](https://bsilverstrim.blogspot.com.au/2016/03/check-raspbian-filesystemdisk-on.html)


### Setup SSH Access

Run the following as the pi user:

```bash
ssh-keygen -t rsa -b 4096

# Add your public ssh key to:
vi /home/pi/.ssh/authorized_keys
chmod 600 /home/pi/.ssh/authorized_keys

sudo vi /etc/ssh/sshd_config
```

Change the following line in the file:
```
PasswordAuthentication no
```

### Install

Install this repository on the Raspberry Pi. Remember to select the applicable branch...

```bash
sudo bash
apt-get install git
cd /root
git clone -b master https://gitlab.com/peterkevans/raspberrypi-environmon.git environmon
sh /root/environmon/install.sh
```


## License

Copyright (c) 2015 Peter K Evans <pete@cysec.net>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
